# Linear System Solutions in R

Jacobi and Gauss-Seidel methods of finding solutions to linear systems, implemented in an R script.

## Usage
1. Open a command-line terminal and navigate to the directory where the script is located.
2. Open an R console in your command-line terminal (`R` for Linux and macOS, `R.exe` for Windows).
3. Run the R script using the following command:
    ```r
        source("LinearSystemSolutions.r")
    ```
4. Select a method for finding the solution of the linear system.
5. Enter the size of the coefficient matrix.
6. Enter the values in the coefficient matrix, one by one, in row-major order.
7. Enter the values in the constant matrix.
8. Enter the initial values for the solution matrix.
9. Specify an error tolerance value.
10. Iterates will be calculated until a suitable solution set is found. If a solution set is not found, an error message will display.
11. You can choose to do another calculation after the previous calculation is done.

## Sample

![Sample](sample.png)