# An R script for solving systems of linear equations,
#     using Jacobi or Gauss-Seidel methods

# Jacobi Method for solving systems of linear equations
jacobi <- function(a, b, x0, tol, itr = 10000) {
    foundSolution <- FALSE
    k <- 0

    d <- diag(c(diag(a)))
    nd <- a - d
    x <- 0

    cat("Iterate  0 : ")
    cat(format(x0, nsmall = 9),"\n")

    for (i in 1:itr) {
        # Solve for new x values and calculate relative error
        x <- c(solve(d) %*% (b - ((nd) %*% x0)))
        rel_err <- norm(as.matrix(x - x0), "i") / (norm(as.matrix(x), "i") + .Machine$double.eps)

        # Display new x values and set for next iterate
        cat("Iterate ", i, ": ")
        cat(format(x, nsmall = 9), ", rel_err: ", rel_err, "\n")
        x0 <- x

        # Stop iterating when a suitable solution is found
        if (rel_err < tol){
            foundSolution <- TRUE
            cat("Solution found:\n")
            for (i in 1:length(b)) {
                cat("x_", i, "= ", x[i], "\n")
            }
            break
        }
    }

    if (!foundSolution)
        cat("No solution found within reasonable number of iterations.\n")
}

# Gauss-Seidel Method for solving systems of linear equations
seidel <- function(a, b, x0, tol, itr = 10000) {
    foundSolution <- FALSE
    k <- 0
    n <- length(b)
    x <- x0

    # Display iterate at k = 0
    cat("Iterate ", k, ": ")
    cat(format(x, nsmall = 9), "\n")

    for (k in 1:itr) {
        # x_1 is entirely dependent on previous iterate's x values,
        #     so the calculation is different.
        x[1,1] <- (b[1,1] - a[1, 2:n] %*% x0[2:n,1]) / a[1,1]

        # Calculation for the other x values except the last
        for (i in 2:n - 1) {
            x[i, 1] <- (b[i,1] - a[i, 1:i - 1] %*% x[1:i-1, 1] - a[i, (i+1):n] %*% x0[(i+1):n, 1]) / a[i, i]
        }

        # Calculation for last x value
        x[n] <- (b[n, 1] - a[n, 1:n-1] %*% x[1:n-1, 1]) / a[n,n]

        # Compute relative error
        rel_err <- norm(as.matrix(x - x0), "i") / (norm(as.matrix(x), "i") + .Machine$double.eps)

        # Display succeeding iterates and set x value for next iteration
        cat("Iterate ", k, ": ")
        cat(format(x, nsmall = 9), ", rel_err: ", rel_err, "\n")
        x0 <- x

        # Stop iterating when a suitable solution is found
        if (rel_err < tol) {
            foundSolution <- TRUE
            cat("Solution found:\n")
            for (i in 1:n) {
                cat("x_", i, "= ", x[i], "\n")
            }
            break
        }
    }

    if (!foundSolution)
        cat("No solution found within reasonable number of iterations.\n")
}

# Function for testing if a matrix is diagonally dominant
isDiagDominant <- function(a, size) {
    for (i in 1:size) {
        sum <- 0

        for (j in 1:size)
            sum <- sum + abs(as.numeric(a[i,j]))

        sum <- sum - abs(as.numeric(a[i,i]))

        if (abs(as.numeric(a[i,i])) < sum)
            return(0)
        else
            return(1)
    }
}

again <- TRUE
while(again) {
    # Prompt user to select method
    cat("[1] Jacobi Method\n")
    cat("[2] Gauss-Seidel Method\n")
    cat("Select Method: ")
    choice <- readLines("stdin", n = 1)

    if (choice == '1' || choice == '2') {
        cat("Enter size of A: ")
        size <- as.numeric(readLines("stdin", n = 1))

        # Matrix size check
        if (size < 2) {
            cat("Error: size must be 2 or more.\n")
        } else {
            a <- matrix(nrow = size, ncol = size)
            # Get values for A matrix
            for (i in 1:size) {
                for (j in 1:size) {
                    cat("Enter values for a_", i, j, ": ")
                    a[i,j] <- as.numeric(readLines("stdin", n = 1))
                }
            }
            cat("\n")

            # Check for diagonal dominance and proceed accordingly
            if (isDiagDominant(a, size) == 1) {
                b <- matrix(nrow = size, ncol = 1)
                # Get values for B matrix
                for (i in 1:size) {
                    cat("Enter value ", i, " for b: ")
                    b[i,1] <- as.numeric(readLines("stdin", n = 1))
                }
                cat("\n")
                # Get initial values
                x0 <- matrix(nrow = size, ncol = 1)
                for (i in 1:size) {
                    cat("Enter x_0[", i, "]: ")
                    x0[i,1] <- as.numeric(readLines("stdin", n = 1))
                }
                cat("\n")
                # Get tolerance value
                cat("Enter error tolerance value: ")
                tol <- as.numeric(readLines("stdin", n = 1))
                cat("\n")

                # Call appropriate function according to user choice
                if (choice == '1')
                    jacobi(a, b, x0, tol)
                else if (choice == '2')
                    seidel(a, b, x0, tol)
            } else {
                if (choice == '2') {
                    cat("Note: matrix is not diagonally-dominant. Results with the Gauss-Seidel method may not be accurate.\n")
                    
                    b <- matrix(nrow = size, ncol = 1)
                    # Get values for B matrix
                    for (i in 1:size) {
                        cat("Enter value ", i, " for b: ")
                        b[i,1] <- as.numeric(readLines("stdin", n = 1))
                    }
                    cat("\n")
                    # Get initial values
                    x0 <- matrix(nrow = size, ncol = 1)
                    for (i in 1:size) {
                        cat("Enter x_0[", i, "]: ")
                        x0[i,1] <- as.numeric(readLines("stdin", n = 1))
                    }
                    cat("\n")
                    # Get tolerance value
                    cat("Enter error tolerance value: ")
                    tol <- as.numeric(readLines("stdin", n = 1))
                    cat("\n")
                    seidel(a, b, x0, tol)
                } else
                    cat("Error: matrix is not diagonally-dominant, please try again.\n")
            }
        }
    } else {
        cat("Invalid choice, please try again.\n")
    }
    cat("\n")

    # Prompt user if they want to do another calculation
    cat("Press 1 to try again: ")
    doAgain <- readLines("stdin", n = 1)

    ifelse(doAgain == "1", again <- TRUE, again <- FALSE)
}
